// nodist

/**
 * @class
 */
class JSRender {
    static addTemplate(name, templateString) {
        JSRender._templates[name] = templateString;
    }

    /** @private */
    static _getEndOfIfBlock(str, startIndex) {
        // Move past start of if block.
        startIndex = str.indexOf('[[if', startIndex);
        if (startIndex === -1) {
            throw new Error('Invalid start of if block in string: ' + str);
        }
        startIndex = str.indexOf(']]', startIndex) + 2;
        if (startIndex === 1) {
            throw new Error('Invalid start of if block in string: ' + str);
        }
        let blocksOpen = 1;
        let nextIf, nextEndIf;

        while (blocksOpen > 0) {
            nextIf = str.indexOf('[[if ', startIndex);
            nextEndIf = str.indexOf('[[endif]]');
            if (nextIf !== -1 && nextIf < nextEndIf) {
                startIndex = str.indexOf('[[if ', startIndex);
                startIndex = str.indexOf(']]', startIndex) + 2;
                blocksOpen++;
            }
            else if (nextEndIf !== -1) {
                startIndex = str.indexOf('[[endif]]', startIndex) + 9;
                blocksOpen--;
            }
            else {
                throw new Error('Unclosed if block in string: ' + str);
            }
        }

        return startIndex - 9;

    }

    /** @private */
    static _getIfContents(str, startIndex) {
        const endIndex = JSRender._getEndOfIfBlock(str, startIndex);
        // Move past start of if block.
        let startIndexInner = str.indexOf('[[if', startIndex);
        if (startIndexInner === -1) {
            throw new Error('Invalid start of if block in string: ' + str);
        }
        startIndexInner = str.indexOf(']]', startIndexInner) + 2;
        if (startIndexInner === 1) {
            throw new Error('Invalid start of if block in string: ' + str);
        }

        return {
            string: str.slice(startIndexInner, endIndex),
            startIndex: startIndex,
            endIndex: endIndex + 9
        }
    }

    /** @private */
    static _render(str, params) {
        let value, match, key;

        while((match = JSRender._pattern.exec(str)) != null) {
            key = match[0].slice(2, match[0].length-2);

            // Handle if blocks.
            if (key.slice(0, 3) === 'if ') {
                key = key.slice(3);

                const substr = JSRender._getIfContents(str, match.index);
                let contents = '';
                if (params[key]) {
                    // Render substr.
                    contents = JSRender._render(substr.string, params);
                }
                str = str.slice(0, substr.startIndex) + contents + str.slice(substr.endIndex);
            }
            // Fill in values.
            else {
                if (key in params) {
                    value = params[key];
                }
                else {
                    value = 'Error: Missing key: ' + key;
                }
                str = str.replace(match[0], value);
            }
        }

        return str;
    }

    /**
     * Renders an html template.
     * @param {string} templateId - The id of the template element.
     * @param {Object} params - The arguments to use.
     */
    static render(templateId, params) {
        let template;
        try {
            template = document.getElementById(templateId).innerHTML;
        }
        catch (e) {
            template = JSRender._templates[templateId];
        }

        if (typeof template !== 'string') {
            throw new Error('No template found with ID: ' + templateId);
        }

        if (!template) return '';
        
        return JSRender._render(template, params);
    }
}

JSRender._templates = {};
JSRender._pattern = /\[\[[a-zA-Z0-9_ ]+\]\]/;

/**
 * @param {string} templateId
 * @param {Object[]} objs
 */
function renderAll(templateId, objs) {
    let html = '';

    objs.forEach((obj) => {
        html = html + JSRender.render(templateId, obj);
    });

    return html;
}