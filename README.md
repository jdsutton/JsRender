# JsRender
Dead-simple client side template rendering.

HTML:
```
<template id="myTemplate">
  <div>[[value1]]</div>
  <div>[[if value2]][[value3]][[endif]]</div>
</template>
```

JS:
```
const rendered = JSRender.render("myTemplate", {
  value1: 'Hello',
  value2: true,
  value3: 'World!
};
